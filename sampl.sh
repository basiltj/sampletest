echo "**********************************************************"
echo Scanning PHP Compatibility
sleep 2
phpVersion=$(php --version | head -n 1 | cut -d " " -f 2 | cut -c 1,3)
if [ $phpVersion -ge 70 ]
then 
	echo PHP version is compatible!!!.
	echo "**********************************************************"
	echo Scanning Apache compatibility
	apacheVersion=$(apache2 -v | grep -i version | cut -d ':' -f 2 | cut -d '/' -f 2 | cut -b 1,2,3)
	sleep 2
	minApacheVersion=2.0
	if [ 1 -eq "$(echo "${minApacheVersion} < $apacheVersion" | bc)" ]
	then
		echo Apache version is compatible!!!.
	        echo "**********************************************************"
		cd /var/www/html
                read -p "Please enter the project name: " projectName
                sudo mkdir $projectName
                sudo chown -R $USER:www-data $projectName/
                cd $projectName
                git clone https://gitlab.com/abhiramh1/laravel.git
                sudo mv laravel sourceCode
                cd sourceCode
                composer install
                mv .env.example .env
                php artisan key:generate
                cd app
                mkdir -p Services Repositories Models
                cd ../
                php artisan config:cache
                php artisan view:clear
                php artisan route:clear
                composer dump-autoload -o
                echo /app/Console/Commands/ICommand.php>>".gitignore"
                echo /app/Console/Commands/ICommandService.php>>".gitignore"
                echo /app/Console/Commands/ICommandRepository.php>>".gitignore"
	else
		echo "Apache is either not installed in your system or the version is outdated. Please update Apache to latest version and continue."
	fi

else
        echo "PHP is either not installed in your system or the version is outdated. Please update PHP to latest version and continue."
fi




